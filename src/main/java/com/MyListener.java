package com;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        new UdpReceiveData(18890).startRun();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
